/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.varios;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import javax.swing.*;

/**
 *
 * @author OMAR
 */
public class ConvertToPdf {

    public static void printFrameToPDF(JFrame componente) {
        String nombreArchivo = "arbol-binario.pdf";

        try {

            com.lowagie.text.Rectangle tamanio = PageSize.A3.rotate();

            Document document = new Document(tamanio);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(nombreArchivo));
            document.open();

            document.addAuthor("Pues yo");
            document.addCreator("openpdf-1.2.16");
            document.addTitle("Arbol Digital");
            document.addCreationDate();
            document.addSubject("Exportacion de arbol digital");

            PdfContentByte cb = writer.getDirectContent();
            PdfTemplate template = cb.createTemplate(tamanio.getWidth(), tamanio.getHeight());
            cb.addTemplate(template, 0, 0);

            Graphics2D g2d = template.createGraphics(tamanio.getWidth(), tamanio.getHeight());
            g2d.scale(0.5, 0.5);

            for (int i = 0; i < componente.getContentPane().getComponents().length; i++) {
                Component c = componente.getContentPane().getComponent(i);
                if (c instanceof JScrollPane) {
                    c.setBounds(0, 0, (int) tamanio.getWidth() * 2, (int) tamanio.getHeight() * 2);
                    c.paintAll(g2d);
                    c.addNotify();
                }
            }

            g2d.dispose();

            document.close();
            writer.close();

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Guardar archivo en ...");
            fileChooser.setSelectedFile(new File("arbol-binario.pdf"));

            int userSelection = fileChooser.showSaveDialog(componente);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();

                File file = new File(nombreArchivo);

                try {
                    Files.copy(file.toPath(), fileToSave.toPath());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.toString());
        }
    }
}
