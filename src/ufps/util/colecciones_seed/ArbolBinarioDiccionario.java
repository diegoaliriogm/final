package ufps.util.colecciones_seed;

import java.util.Iterator;
import ufps.util.Diccionario;
import ufps.util.Letra;

public class ArbolBinarioDiccionario<T> extends ArbolBinario<T> {
    
    Diccionario diccionario = new Diccionario();
    
    public ArbolBinarioDiccionario() {
        super();
    }
    
    public ArbolBinarioDiccionario(T raiz) {
        super(raiz);
    }
    
    public void llenarArbol(String linea, short posicionY) {
        
        short posicionX = 0;
        NodoBin<T> nodoExiste;
        char[] letras = linea.toCharArray();
        char letra;
        
        for (short i = 0; i < letras.length; i++) {
            
            if (isCh(i, letras)) {
                letra = '#';
                i++;
            } else {
                letra = letras[i];
            }
            
            nodoExiste = this.buscarNodo(letra);
            if (nodoExiste == null) {
                
                boolean[] binarios = diccionario.buscar(letra);
                NodoBin<T> nodo = super.raiz;
                boolean derecho = false;
                
                for (boolean binario : binarios) {
                    if (!binario) { // 0
                        if (nodo.getIzq() == null) {
                            nodo.setIzq(new NodoBin<>((T) new Letra('$', false)));
                        }
                        nodo = nodo.getIzq();
                        derecho = false;
                    } else { // 1
                        if (nodo.getDer() == null) {
                            nodo.setDer(new NodoBin<>((T) new Letra('$', true)));
                        }
                        nodo = nodo.getDer();
                        derecho = true;
                    }
                }
                nodo.setInfo((T) new Letra(letra, derecho, posicionX, posicionY));
            } else {
                Letra info = (Letra) nodoExiste.getInfo();
                info.addPosicion(posicionX, posicionY);
                nodoExiste.setInfo((T) info);
            }
            
            posicionX++;
        }
        
    }
    
    private boolean isCh(short posicion, char[] letras) {
        
        try {
            if (letras[posicion] == 'c' && letras[posicion + 1] == 'h') {
                return true;
            }
        } catch (Exception e) {
        }
        
        return false;
    }
    
    public ArbolExpresionGrafico getDibujo() {
        return new ArbolExpresionGrafico(this);
    }
    
    public int buscarCoincidencias(String palabra) {
        
        palabra = formatearTexto(palabra);
        desmarcarCoincidencias();
        
        char[] letras = palabra.toCharArray();
        int total = 0;
        
        try {
            NodoBin<T> primerNodo = buscarNodo(letras[0]);
            Letra primeraLetra = (Letra) primerNodo.getInfo();
            boolean existe;
            for (short[] posiciones : primeraLetra.getPosiciones()) {
                existe = buscarCoincidencias(letras, 1, posiciones[0] + 1, posiciones[1]);
                if (existe) {
                    posiciones[2] = 1;
                    marcarCoincidencias(letras, 1, posiciones[0] + 1, posiciones[1]);
                    total++;
                }
            }
            
            if (total > 0) {
                marcarRama(primeraLetra);
            }
            
        } catch (Exception e) {
        }
        
        return total;
    }
    
    private String formatearTexto(String texto) {
        String formato = texto.toLowerCase();
        
        return formato.replace("ch", "#");
    }
    
    private void desmarcarCoincidencias() {
        Letra letra;
        for (Iterator iterator = this.preOrden(); iterator.hasNext();) {
            letra = (Letra) iterator.next();
            if (letra != null) {
                letra.limpiarCoincidencias();
            }
        }
        
    }
    
    private boolean buscarCoincidencias(char[] letras, int posicion, int posicionX, int posicionY) {
        
        if (letras.length == posicion) {
            return true;
        }
        
        NodoBin<T> nodo = buscarNodo(letras[posicion]);
        
        if (nodo == null) {
            return false;
        }
        
        Letra letra = (Letra) nodo.getInfo();
        
        for (short[] posiciones : letra.getPosiciones()) {
            if (posiciones[0] == posicionX && posiciones[1] == posicionY) {
                return buscarCoincidencias(letras, posicion + 1, posicionX + 1, posicionY);
            }
        }
        return false;
        
    }
    
    private boolean marcarCoincidencias(char[] letras, int posicion, int posicionX, int posicionY) {
        
        if (letras.length == posicion) {
            return true;
        }
        
        NodoBin<T> nodo = buscarNodo(letras[posicion]);
        
        if (nodo == null) {
            return false;
        }
        
        Letra letra = (Letra) nodo.getInfo();

        marcarRama(letra);
        for (short[] posiciones : letra.getPosiciones()) {
            if (posiciones[0] == posicionX && posiciones[1] == posicionY) {
                
                if (marcarCoincidencias(letras, posicion + 1, posicionX + 1, posicionY)) {
                    posiciones[2] = 1;
                    return true;
                }
            }
        }
        return false;
        
    }
    
    private void marcarRama(Letra letra) {
        try {
            boolean[] binarios = diccionario.buscar(letra.getLetra());
            NodoBin<T> nodo = super.raiz;
            
            for (boolean binario : binarios) {
                if (!binario) { // 0
                    nodo = nodo.getIzq();
                } else { // 1
                    nodo = nodo.getDer();
                }
                if (nodo != null) {
                    Letra l = (Letra) nodo.getInfo();
                    l.setNodoColor(true);
                    nodo.setInfo((T) l);
                }
            }

        } catch (Exception e) {
        }
    }
    
    private NodoBin<T> buscarNodo(char letra) {
        try {
            boolean[] binarios = diccionario.buscar(letra);
            NodoBin<T> nodo = super.raiz;
            
            for (boolean binario : binarios) {
                if (!binario) { // 0
                    nodo = nodo.getIzq();
                } else { // 1
                    nodo = nodo.getDer();
                }
            }
            return nodo;
            
        } catch (Exception e) {
        }
        
        return null;
    }
    
}
