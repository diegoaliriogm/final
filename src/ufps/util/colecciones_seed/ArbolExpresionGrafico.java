package ufps.util.colecciones_seed;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import ufps.util.Letra;

public class ArbolExpresionGrafico<T> extends JPanel {

    private final int MAX_VALUE = Integer.MAX_VALUE;

    private final Color COLOR_RAMA_ACTIVA = Color.RED;
    private final Color COLOR_RAMA = Color.BLACK;

    private final Color COLOR_HOJA_ACTIVA = Color.BLUE;
    private final Color COLOR_HOJA = Color.BLACK;

    private final Stroke ANCHO_ACTIVO = new BasicStroke(2);
    private final Stroke ANCHO = new BasicStroke(1);

    private final ArbolBinario<T> miArbol;
    private HashMap posicionNodos = null;
    private HashMap subtreeSizes = null;
    private boolean dirty = true;
    private final int parent2child = 20, child2child = 30;
    private final Dimension empty = new Dimension(0, 0);
    private FontMetrics fm = null;

    private int ancho;

    public ArbolExpresionGrafico(ArbolBinario<T> miArbol) {
        this.miArbol = miArbol;
        this.setBackground(Color.WHITE);
        posicionNodos = new HashMap();
        subtreeSizes = new HashMap();
        dirty = true;
        repaint();
    }

    private void calcularPosiciones() {
        posicionNodos.clear();
        subtreeSizes.clear();
        NodoBin<T> root = this.miArbol.getRaiz();
        if (root != null) {
            calcularTamañoSubarbol(root);
            calcularPosicion(root, MAX_VALUE, MAX_VALUE, 0);
        }
    }

    private Dimension calcularTamañoSubarbol(NodoBin<T> n) {
        if (n == null) {
            return new Dimension(0, 0);
        }

        Dimension ld = calcularTamañoSubarbol(n.getIzq());
        Dimension rd = calcularTamañoSubarbol(n.getDer());

        int h = fm.getHeight() + parent2child + Math.max(ld.height, rd.height);
        int w = ld.width + child2child + rd.width;

        Dimension d = new Dimension(w, h);
        subtreeSizes.put(n, d);

        return d;
    }

    private void calcularPosicion(NodoBin<T> n, int left, int right, int top) {
        if (n == null) {
            return;
        }

        Dimension ld = (Dimension) subtreeSizes.get(n.getIzq());
        if (ld == null) {
            ld = empty;
        }

        Dimension rd = (Dimension) subtreeSizes.get(n.getDer());
        if (rd == null) {
            rd = empty;
        }

        int center = 0;

        if (right != MAX_VALUE) {
            center = right - rd.width - child2child / 2;
        } else if (left != MAX_VALUE) {
            center = left + ld.width + child2child / 2;
        }
        int width = fm.stringWidth("#####");
        int height = fm.getHeight() * (n.getInfo().toString().split("\n").length);

        posicionNodos.put(n, new Rectangle(center - width / 2 - 3, top, width + 6, height));

        calcularPosicion(n.getIzq(), MAX_VALUE, center - child2child / 2, top + height + parent2child);
        calcularPosicion(n.getDer(), center + child2child / 2, MAX_VALUE, top + height + parent2child);
    }

    private void dibujarArbol(Graphics2D g, NodoBin<T> n, int puntox, int puntoy, int yoffs) {
        if (n == null) {
            return;
        }
        Letra l = (Letra) n.getInfo();

        Rectangle r = (Rectangle) posicionNodos.get(n);
        if (l.letraCoincide()) {

            g.setColor(COLOR_HOJA_ACTIVA);
            g.setStroke(ANCHO_ACTIVO);
            g.draw(r);
            g.setColor(COLOR_HOJA);
            g.setStroke(ANCHO);

        } else {

            if (l.isNodoColor()) {

                g.setColor(COLOR_RAMA_ACTIVA);
                g.setStroke(ANCHO_ACTIVO);
                g.draw(r);
                g.setColor(COLOR_RAMA);
                g.setStroke(ANCHO);

            } else {
                g.draw(r);

            }

        }

        drawString(g, l.toString(), r.x + 3, r.y + yoffs);

        if (puntox != MAX_VALUE) {
            if (l.isNodoColor() || l.letraCoincide()) {
                g.setColor(COLOR_RAMA_ACTIVA);
                g.setStroke(ANCHO_ACTIVO);
                g.drawLine(puntox, puntoy, (int) (r.x + r.width / 2), r.y);
                g.setStroke(ANCHO);
                g.setColor(COLOR_RAMA);
            } else {
                g.drawLine(puntox, puntoy, (int) (r.x + r.width / 2), r.y);
            }
        }

        dibujarArbol(g, n.getIzq(), (int) (r.x + r.width / 2), r.y + r.height, yoffs);
        dibujarArbol(g, n.getDer(), (int) (r.x + r.width / 2), r.y + r.height, yoffs);

    }

    void drawString(Graphics g, String text, int x, int y) {
        if (text.contains("\n")) {
            for (String line : text.split("\n")) {
                g.drawString(line, x, y);
                y += g.getFontMetrics().getHeight();
            }
        } else {
            g.drawString(text, x, y);
        }

    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        fm = g.getFontMetrics();

        if (dirty) {
            calcularPosiciones();
            dirty = false;
        }

        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(getWidth() / 2, parent2child);
        dibujarArbol(g2d, this.miArbol.getRaiz(), MAX_VALUE, MAX_VALUE,
                fm.getLeading() + fm.getAscent());
        fm = null;
    }

    public int getAncho() {
        return ancho;
    }
}
