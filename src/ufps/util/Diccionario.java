package ufps.util;

import java.util.HashMap;
import java.util.Map;

public class Diccionario {

    private Map<Character, boolean[]> lista;

    public Diccionario() {
        lista = new HashMap<>();

        lista.put('a', new boolean[]{false, false, false, false, false, true});
        lista.put('b', new boolean[]{false, false, false, false, true, false});
        lista.put('c', new boolean[]{false, false, false, false, true, true});
        lista.put('#', new boolean[]{false, false, false, true, false, false});//ch
        lista.put('d', new boolean[]{false, false, false, true, false, true});
        lista.put('e', new boolean[]{false, false, false, true, true, false});
        lista.put('f', new boolean[]{false, false, false, true, true, true});
        lista.put('g', new boolean[]{false, false, true, false, false, false});
        lista.put('h', new boolean[]{false, false, true, false, false, true});
        lista.put('i', new boolean[]{false, false, true, false, true, false});
        lista.put('j', new boolean[]{false, false, true, false, true, true});
        lista.put('k', new boolean[]{false, false, true, true, false, false});
        lista.put('l', new boolean[]{false, false, true, true, false, true});
        lista.put('m', new boolean[]{false, false, true, true, true, false});
        lista.put('n', new boolean[]{false, false, true, true, true, true});
        lista.put('ñ', new boolean[]{true, true, false, false, false, false});
        lista.put('o', new boolean[]{true, true, false, false, false, true});
        lista.put('p', new boolean[]{true, true, false, false, true, false});
        lista.put('q', new boolean[]{true, true, false, false, true, true});
        lista.put('r', new boolean[]{true, true, false, true, false, false});
        lista.put('s', new boolean[]{true, true, false, true, false, true});
        lista.put('t', new boolean[]{true, true, false, true, true, false});
        lista.put('u', new boolean[]{true, true, false, true, true, true});
        lista.put('v', new boolean[]{true, true, true, false, false, false});
        lista.put('w', new boolean[]{true, true, true, false, false, true});
        lista.put('x', new boolean[]{true, true, true, false, true, false});
        lista.put('y', new boolean[]{true, true, true, false, true, true});
        lista.put('z', new boolean[]{true, true, true, true, false, false});
        lista.put('/', new boolean[]{true, true, true, true, false, true});//salto de linea
        lista.put(' ', new boolean[]{true, true, true, true, true, false});// espacio
    }

    public Map<Character, boolean[]> getLista() {
        return lista;
    }

    public void setLista(Map<Character, boolean[]> lista) {
        this.lista = lista;
    }
    
    public boolean[] buscar(Character caracter) {
        return lista.get(caracter);
    }

}
